package URI::rtspu;

use strict;
use warnings;

our $VERSION = '5.20';

use parent 'URI::rtsp';

sub default_port { 554 }

1;
